<?php
    require_once("animal.php");
    require_once("buduk.php");
    require_once("kerasakti.php");
    
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>". "<br>"; // "no"

$buduk = new buduk("Kodok Budug");
echo "Name : " . $buduk->name . "<br>";
echo "Legs : " . $buduk->legs . "<br>";
echo "Cold Blooded : " . $buduk->cold_blooded . "<br>";
echo $buduk->lompat();



$kerasakti = new kerasakti("Kera Sakti");
echo "Name : " . $kerasakti->name . "<br>";
echo "Legs : " . $kerasakti->legs . "<br>";
echo "Cold Blooded : " . $kerasakti->cold_blooded . "<br>";
echo $kerasakti->yell();



?>